+++
title = 'About RSE'
toc = true
+++

## What is a Research Software Engineer?

Are you wondering what a research software engineer is, or if
you are one? The
[definition](https://us-rse.org/about/what-is-an-rse)
given by the [US-RSE] is:

>    We like an inclusive definition of Research Software Engineers to
>    encompass those who regularly use expertise in programming to
>    advance research. This includes researchers who spend a significant
>    amount of time programming, full-time software engineers writing
>    code to solve research problems, and those somewhere in-between. We
>    aspire to apply the skills and practices of software development to
>    research to create more robust, manageable, and sustainable
>    research software.


## A bit of history

Our initiative builds on 10 years of work by others.

The {{< abbrev "SSI" >}} conducted a study in 2014 to access the importance
of Software in UK Research.
The conclusion was that around 70% of researchers said they could not carry out
their research without *research software*[^1].
Another finding was that most researchers who develop research software have no
formal training in software development.
[This blog post](https://www.software.ac.uk/blog/its-impossible-conduct-research-without-software-say-7-out-10-uk-researchers)
discusses the results of this study in more detail.

The first action taken to highlight the special role of the programmers who
develop research software was to create a new and unique name: **Research
Software Engineers**.

We recommend [this article](https://www.software.ac.uk/blog/not-so-brief-history-research-software-engineers-0)
for a full overview.


[^1]: <q>Software that does not generate, process or analyze results - such as word
processing software, or the use of a web search - does not count as 'research
software' for the purposes of this survey.</q>
