---
title: On-site Meeting April 2024
sidebar:
  exclude: true
---


The next on-site event is on Wednesday April 24, 16-18 at
[CAB G 61](http://www.mapsearch.ethz.ch/map.do?gebaeudeMap=CAB&farbcode=c010&lang=en)
in Zurich.

We are looking for **speakers** for this event, either for a 5-minute
**lightning talk** or a more extended **20-minute presentation + 10-minute Q&A session**.

If you would like to attend the event, please register using https://u.ethz.ch/6931D.
You can also submit a presentation proposal using the same registration form.
