---
title: Zoom Meeting March 2024
sidebar:
  exclude: true
---

Dear all,

We are pleased to announce the next RSE community meeting on Tuesday, **12th of
March 16:00-17:30** on
[Zoom](https://ethz.zoom.us/j/61656888562?pwd=QXpMZ2U1OFBzLzBOSlJ5ZzdBRWNMdz09).

The agenda for the meeting is:

-   16:00 – 16:15:  Introduction, including
    -   Update on our recent activities
    -   Summary of the group discussions from the meeting on the 1st of February
    -   Our proposal for the next steps
-   16:15 – 16:40: Lightning talks
-   16:40 – 16:50: Break
-   16:50 – 17:30: Presentation by Matthias Bannert from KOF ETH: “Scientific and Technical Publishing with [Quarto](https://quarto.org/)  -- from Reports to Books and Websites” + Q&A


Please feel free to forward this email,

We are looking forward to this event,

Tarun, Franziska and Uwe.
