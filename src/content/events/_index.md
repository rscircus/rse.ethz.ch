+++
title = 'Events'
toc = true
+++

## Upcoming events

{{< include "events/upcoming.md" >}}

## Past events

| Date &amp; Time | Location | Event |
|-----------------|----------|-------|
| 12th of March 2024 <br/> 16:00-17:30 <br/> | Zoom | [Online meeting](second_meeting) |
| 1st of February 2024 <br/>15:00-17:30 | HG D 3.2 | First **on-site** event at ETHZ. [Details here](first_onsite_event). |
| 12th of December 23<br/> 16:00-17:30 | zoom | RSE information event with [Prof. Simon Hettrick](https://www.software.ac.uk/our-people/simon-hettrick) ([slides](https://bit.ly/3TsPrrB)) and presentation of our ideas for first steps towards an RSE community at ETH Zurich. |
