| Date &amp; Time | Location | Event |
|-----------------|----------|-------|
| 24th of April 2024 16:00 - 18:00 | [CAB G 61](http://www.mapsearch.ethz.ch/map.do?gebaeudeMap=CAB&farbcode=c010&lang=en)  | On-site meeting ([more details here](/events/2024_04_meetup))|
