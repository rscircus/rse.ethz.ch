---
title: First on-site event
sidebar:
  exclude: true
---

With around 60 participants, the **first on-site event** of the RSE
community @ ETH Zurich took place on **Thursday, 1st of February**, from **15:00
to 17:30** at ETH Zurich’s central campus in **HG D3.2**.


The schedule was as follows:

- 15:00 - 15:45 : Lightning talks
- 15:45 - 16:00 : break
- 16:00 - 16:30 : Presentation by  Mr. Peter Schmidt, Trustee of the [RSE society UK](https://society-rse.org/)
- 16:30 - 17:30 : Breakout sessions/discussions


We are going to summarize the results from the discussion rounds soon.

Here are a few pictures from the event:

{{< lightbox src="picture-01.jpeg" group="gallery" size="200x" >}}
{{< lightbox src="picture-02.jpeg" group="gallery" size="200x" >}}
{{< lightbox src="picture-03.jpeg" group="gallery" size="200x" >}}
