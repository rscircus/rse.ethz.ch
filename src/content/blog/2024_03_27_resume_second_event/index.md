+++
title = 'Resume of 2nd RSE@ETHZ event'
date = 2024-03-27T19:44:33+01:00
authors = ['Roland Siegbert']
+++

Our RSE community continues to grow and evolve. The recent establishment of [working groups](https://rse.ethz.ch/blog/2024_03_26_call_working_groups/) and a series of lightning talks highlighted the diverse interests and expertise within our ranks.

<!--more-->

In our second event -- enjoyed and joined by roughly 90 RSEs -- we built upon the foundation established previously, with discussions focused on refining our working group structure and outlining our community's future direction.

### Lightning Talk Highlights & a Deep Dive into Quarto

Following this intro, a set of lightning talks were held with the following highlights:

- _Solid_dmft: Grey-boxing Correlated Materials_ - Alberto Carta and Peter Mlkvik presented their experience with RSE in their field.
- _Brightway Software Framework: From Scattered Pages to Interactive Documentation_ - Michael Weinold demonstrated how the Brightway framework can improve technical documentation processes.
- _Research Software Engineering Automating Workflows_ - Data Scientist [Roman Wixinger](https://www.engineersforscience.com/blog) discussed the value of RSE in streamlining scientific workflows.
- _Increasing the scope of applications through user configuration_ - By Loic Hausammann 
- _Speedrun through Retrieval Augmented Generation_ - [Roland Siegbert](https://read.cv/roland) showcased the possibilities of using LLM-assisted search in paper and thesis databases.

Finally, Matthias Bannert presented an in-depth look at Quarto: https://quarto.org/, a powerful and versatile publishing tool. He covered Quarto's capabilities for creating reports, books, websites, and more.

The event concluded with a discussion on the evolution of scientific publishing tools. Participants weighed the strengths of LaTeX, the potential of [Typst](https://typst.app/), and the importance of Markdown for technical writing and in Research Software Engineering.

### Community Engagement

These activities showcase the strength and potential of our RSE community. If you'd like to get involved, consider joining a working group or participating in future events.

