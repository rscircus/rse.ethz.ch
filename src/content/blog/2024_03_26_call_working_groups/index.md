+++
title = 'We need your help: Call for contributions to working groups!'
date = 2024-03-26T19:20:35+01:00
authors = ['Uwe Schmitt']
+++


Based on the group discussions at the first meeting in February, we decided to
initiate working groups.

<!--more-->

The topics we suggest are:
  - **Events**. E.g. help to organize rooms and speakers, setup casual meetings, and more.
    Other ideas and suggestions at https://gitlab.com/rse-ethz/working-group-events)
  - **Outreach**: E.g.  work on website, blog posts, announce RSE community
    in department or research group meetings. See also https://gitlab.com/rse-ethz/working-group-outreach.
  - **RSE training**: E.g. collect and publish training material, offer training activities.
    (https://gitlab.com/rse-ethz/working-group-rse-training)
  - **RSE community survey**: Develop, conduct and evaluate a survey of the RSE community
    (https://gitlab.com/rse-ethz/working-group-community-survey)


The working groups are an opportunity for anyone interested in actively shaping
and developing the RSE community. We do not expect any experience with this
type of work and recognize that this is a voluntary role.

Please fill out the form (https://u.ethz.ch/S6xMK) if you are interested in
contributing your ideas, sharing and **collaborating with like-minded people**, and
maybe trying something you have never done before. We would then arrange
initial meetings to get the working groups started.
