+++
date = 2024-02-18T13:52:55+01:00
toc = true
+++

# Research Software Engineering Community @ ETH Zurich

Software is ubiquitous in modern research and its quality has a direct impact
on the quality of research. The Research Software Engineering Community at ETH
connects research software engineers, increases their visibility and works
towards the recognition of their importance in science.

## Latest blog entries

{{< latest_blog_entries >}}

## Upcoming events

{{< include "events/upcoming.md" >}}

We announce upcoming events and list past events at [Events](/events).

## How to stay in touch?

- To keep up to date with our [events](/events) and other
  activities, please subscribe to our mailing list at
  [https://sympa.ethz.ch/sympa/subscribe/rse-community](https://sympa.ethz.ch/sympa/subscribe/rse-community).
  You will receive an email after registration to confirm and finalize your subscription.

- We also created a chat space `#rse-ch:matrix.org` on \[matrix\]. Please follow this link:
  This link will offer you several options to connect.
  [https://m.ethz.ch/#/#rse-ch:matrix.org](https://m.ethz.ch/#/#rse-ch:matrix.org)

    - In case you work for ETH Zurich you can find some help
      [at the ETH wiki](https://unlimited.ethz.ch/display/itkb/Get+started).
    - If you are a student, you can use the
      [same instructions](https://unlimited.ethz.ch/display/itkb/Get+started)
      but replace `staffchat` by `student`.
    - In all other cases check out if your institution offers matrix accounts, or
      [read the matrix documentation](https://matrix.org/docs/older/introduction/#how-can-i-try-it-out) how to create one.

- For further information, if you have issues to register to the mailing list
  or to connect to the chat, please contact
  <span class="email">uwe.sc<b>asdf</b>hmit<b>d</b>t@id.<b>.</b>ethz.ch</span>.



## Imprint

We are data scientists and software engineers working at the {{< abbrev "SIS" >}}. This
website is an attempt to communicate our current activities and to increase
visibility of the community.
Please email <span class="email">uwe.sc<b>asdf</b>hmit<b>d</b>t@id.<b>.</b>ethz.ch</span>
in case you want to contact us.
