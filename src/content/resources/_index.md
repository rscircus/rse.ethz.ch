---
title: Resources
---

- ### General RSE
    - [10 Reasons to be an RSE](https://www.software.ac.uk/blog/ten-reasons-be-research-software-engineer)
    - [A not-so-brief history of RSEs](https://www.software.ac.uk/blog/not-so-brief-history-research-software-engineers)
    - [Why Science needs more RSEs](https://www.nature.com/articles/d41586-022-01516-2)
    - [Code for Thought podcast](https://codeforthought.buzzsprout.com/)

- ### Training Resources
    - [Software Carpentry Lessons](https://software-carpentry.org/lessons/)
    - [Code Refinery Lessons](https://coderefinery.org/lessons/from-coderefinery/)
    - [Intermediate Research Software Development](https://carpentries-incubator.github.io/python-intermediate-development/)
    - [Byte-sized RSE](https://www.universe-hpc.ac.uk/events/byte-sized-rse/)

- ### RSE Societies
    - [Society of Research Software Engineering UK](https://society-rse.org/)
    - [The United States Research Software Engineer Association](https://us-rse.org/)
    - [German RSE Association](https://de-rse.org/en/)
    - [Nordic-RSE](https://nordic-rse.org/)
    - [NL-RSE](https://nl-rse.org/)
