# About

This is the source for the website at [https://rse.ethz.ch](https://rse.ethz.ch).

The website is built using HUGO and a slightly modified version of the Hextra theme.

## Setup instructions

Tools used:

  - `HUGO` (installation instructions [here](https://gohugo.io/installation/))
    which is used to generate the static web site.
  - `pre-commit` (see [here](https://pre-commit.com/)) for spell checking.

Please install both tools before you work on the website.

To check out the code and initialize your setup, please run:

```
$ git clone https://gitlab.com/rse-ethz/rse.ethz.ch.git
$ cd rse.ethz.ch
$ git submodule init
$ git submodule update
$ pre-commit install
```

## Spellchecker

After setting up the `pre-commit` hook, every `git commit` will spell check
the markdown files included in the commit. You can whitelist words by adding
lines to `spellchecker-ignore.txt`.


## Working on the content of the website

To build the website and start a webserver:

```
$ cd src
$ hugo server --disableFastRender -D
```

This should serve the website at [http://localhost:1313](http://localhost:1313).
Please read the exact output, the port might be different than `1313`.

You can now open another terminal window or tab and edit the content of the website.

The content of the website is in the `src/content` folder. For a more detailed
overview of the folder structure [read here.](https://imfing.github.io/hextra/docs/guide/organize-files/)

### Example: create a new blog post

```
$ cd src
$ hugo new content content/blog/2024_12_24_xmas.md
```

[- WARNING -]

<code>hugo new content</code> only works if you are in the parent folder of
<code>content</code>. Else you may get an error message. But you can also
create markdown files manually!

You can edit this file now. HUGO will detect changes and then build and serve the updated web site.


Comments:

 - Please use underscores and lower case letters for the file names. No spaces, upper case or special characters.
 - You will need to correct the title entry in the markdown file.


## Contribution

Please create merge requests for contribution, the CI server will build and publish the new content after a successful merge.
