#!/usr/bin/env bash
#
#
# from https://stackoverflow.com/questions/59895/
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR/src

hugo server --disableFastRender -D
